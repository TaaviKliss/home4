import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
	   
	   Lfraction murd1 = new Lfraction(5, 4);
	   Lfraction murd2 = new Lfraction(1, 6);	   
	   System.out.println(murd1.equals(murd2));
//	   System.out.println(toLfraction(5.4, 5));
	   System.out.println(valueOf("-8/-4"));
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */  
   
   public Lfraction (long a, long b) {

	   if(b == 0){
		   
	         throw new RuntimeException("Ühel sisestatud murdudest on nimetajaks " + b
	        		 + ", kuid nimetajaks ei tohi olla 0. (LFRACTION)");
	      }	  
	      
	      if (b < 0) {
	    	  
	         a = -a;
	         b = -b;
	   }

	  this.numerator = a;
	  this.denominator = b;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
  
	   return this.numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
	   
	   return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {

	   return Long.toString(numerator) + "/" + Long.toString(denominator);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

	   return (this.compareTo ((Lfraction)m) == 0);
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
	   
//	ENNE:   return (int) (numerator ^ denominator);
	   
	   return (int) ((Math.exp(numerator) * -97) * (Math.exp(denominator) / 41));
   }
   
   public static long leia_yhistegur(long numerator, long denominator) {
	   
	   long yhis_denominator = Math.max(Math.abs(numerator), Math.abs(denominator));
	   
	   if (yhis_denominator == 0)
		   
		   throw new ArithmeticException("Suurima ühisteguri nimetajaks on "
				   + yhis_denominator + ", "
				   		+ "kuid nimetajaks ei tohi olla 0. (LEIA_YHISTEGUR)");
	   
	   long yhis_numerator = Math.min(Math.abs(numerator), Math.abs(denominator));
	   
	   while (yhis_numerator > 0) {
		   
		   numerator = yhis_denominator % yhis_numerator;
	       yhis_denominator = yhis_numerator;
	       yhis_numerator = numerator;
	   }
	   
	   return yhis_denominator;
   }
   
   private Lfraction taanda() {
	   
	   Lfraction murd = null;
	   
      try {
    	  
         murd = (Lfraction)clone();
      } catch (CloneNotSupportedException e) {};
      
      if (denominator == 0)
    	  
         throw new ArithmeticException ("Murru " + murd + " nimetajaks on "
				   + denominator + ", kuid nimetajaks ei tohi olla 0. (TAANDA)");
      
      if (denominator < 0) {
    	  
         murd.numerator = -numerator;
         murd.denominator = -denominator;
      }
      
      if (numerator == 0)
    	  
         murd.denominator = 1;      
      else {
         long yhine_tegur = leia_yhistegur (numerator, denominator);
         murd.numerator = numerator / yhine_tegur;
         murd.denominator = denominator / yhine_tegur;
      }
      return murd;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

	   if ((this.denominator == 0) || (m.denominator == 0))
		   
	         throw new IllegalArgumentException("Murru " + m + " nimetajaks on "
				   + denominator + ", kuid nimetajaks ei tohi olla 0. (LFRACTION PLUS)");
	   
	  return new Lfraction (numerator*m.denominator + denominator*m.numerator,
		         denominator*m.denominator).taanda();
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {

	   if ((this.denominator == 0) || (m.denominator == 0))
		   
	         throw new IllegalArgumentException("Murru " + m + " nimetajaks on "
				   + denominator + ", kuid nimetajaks ei tohi olla 0. (LFRACTION TIMES)");
	   
	   return new Lfraction (numerator*m.numerator, denominator*m.denominator)
		         .taanda();
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
	   
	   // LISATUD
	   if (denominator == 0) {
		   
		   throw new RuntimeException("Murru nimetajaks on " + denominator
				   + ", kuid nimetajaks ei tohi olla 0. (INVERSE)");
	   }
	   // LISATUD

	   if (numerator < 0) {
		   
	         return new Lfraction(-1 * this.taanda().denominator, -1 * this.taanda().numerator);
	      } else {
	         return new Lfraction(this.taanda().denominator, this.taanda().numerator);
	      }
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {

	   return new Lfraction(-numerator, denominator).taanda();
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {

	   if ((this.denominator == 0) || (m.denominator == 0))
		   
	         throw new IllegalArgumentException("Murru " + m + " nimetajaks on "
				   + denominator + ", kuid nimetajaks ei tohi olla 0. (LFRACTION MINUS)");
	   
	  return new Lfraction (
          (numerator * m.getDenominator() - m.getNumerator() * denominator) ,
           denominator * m.getDenominator() );
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {

	   if (m.denominator == 0 || this.denominator == 0) {
		   
		   throw new RuntimeException("Murru " + m + " nimetajaks on "
				   + denominator + ", "
				   		+ "kuid nimetajaks ei tohi olla 0. (LFRACTION DIVIDEBY)");
	   }
	   
	   return this.times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {

	   long yks_murd = numerator * m.denominator;
	   long teine_murd = m.numerator * denominator;

	      if (m.denominator == 0) {
	         throw new RuntimeException("Murru " + m + " nimetajaks on "
				   + denominator + ", kuid nimetajaks ei tohi olla 0. (COMPARETO)");
	      } else {
	         if (yks_murd < teine_murd) {
	            return -1;
	         }
	         else if (yks_murd == teine_murd) {
	            return 0;
	         }
	         else if (yks_murd > teine_murd) {
	            return 1;
	         }
	         else {
	            throw new ClassCastException("Murdu " + yks_murd
	            		+ " ja murdu " + teine_murd + " ei saa võrrelda. (COMPARETO)");
	         }
	   }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

	   return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {

	   Lfraction murd = taanda();
	   return murd.getNumerator()/murd.getDenominator();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {

	   return new Lfraction(numerator - denominator*integerPart(), denominator).taanda();
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {

	   return ((double)(getNumerator())) / ((double)(getDenominator()));
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {

	   if (d > 0) {
		   
	         return new Lfraction((Math.round(f * d)), d);
	      }else{
	         throw  new RuntimeException("Nimetaja " + d + " ei tohi olla võrdne nulliga ega ka olla nullist väiksem. (TOLFRACTION)");
	      }
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {

	   StringTokenizer st = new StringTokenizer (s, "/<>[](),");
	   long numerator = 0;
	   long denominator = 1;
	   
	   if (s.trim().length() == 0) {
		   
		   throw new RuntimeException("Te sisestasite tühja sõne.");
	   }
	   
	   try {
		   
		   if (st.hasMoreTokens()) {
		   
			   numerator = Long.parseLong (st.nextToken().trim());
		   } else {
			   throw new IllegalArgumentException ("Antud murd " + s + " on ebakorrektne. (VALUEOF)");
		   }	   
		   
	   }catch (Exception e) {
		   throw new IllegalArgumentException ("Antud murd " + s + " on ebakorrektne. (VALUEOF)");
	   }
	   
	   try {
		   
		   if (st.hasMoreTokens()) {
		   
			   denominator = Long.parseLong (st.nextToken());
		   } else {
			   denominator = 1;
		   }
	   } catch (Exception e) {
		   throw new IllegalArgumentException ("Antud murd " + s + " on ebakorrektne. (VALUEOF)");
	   }
	   
	   if (st.hasMoreTokens()) {
		   
		   throw new IllegalArgumentException ("Antud murd " + s + " on ebakorrektne. (VALUEOF)");
	   }
	   
	      return new Lfraction (numerator, denominator);
   }
}

